.DEFAULT_GOAL := up

.PHONY: up
up:
	$(MAKE) down
	docker-compose up -d
	$(MAKE) logs

.PHONY: down
down:
	docker-compose down --remove-orphans
	$(MAKE) _clean

.PHONY: build
build:
	$(MAKE) down
	docker-compose build
	$(MAKE) up

.PHONY: shell
shell:
	docker exec -it mvf-logger sh

.PHONY: test
test:
	docker exec -it mvf-logger sh -c "npm test -- -t $(filter)"

.PHONY: logs
logs:
	docker-compose logs -f --tail=100

.PHONY: _clean
_clean:
	docker volume rm mvf-logger_dist || true
	rm -fr dist || true

.PHONY: publish
publish:
	npm run compile && npm test 
	npm version $(kind)
	cp ./package.json ./dist
	npm publish ./dist && rm -fr ./dist
	git push && git push --tags