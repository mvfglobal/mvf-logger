FROM node:14.17-alpine

ENV SHELL=/bin/bash

RUN apk add --no-cache bash curl
RUN npm install -g typescript

WORKDIR /app

COPY package-lock.json package-lock.json
COPY package.json package.json

RUN npm install

COPY . .

USER node
