enum Format {
  COMBINED = ':remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent"',
  COMMON = ':remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length]',
  DEV = ':method :url :status :response-time ms - :res[content-length]',
  SHORT = ':remote-addr :remote-user :method :url HTTP/:http-version :status :res[content-length] - :response-time ms',
  TINY = ':method :url :status :res[content-length] - :response-time ms',
}

export default Format;
