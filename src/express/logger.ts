import { IncomingMessage, ServerResponse } from 'http';
import morgan, { compile, TokenIndexer } from 'morgan';
import Format from '../server/Format';
import { info, warning, error } from '..';

type ExpressLogger = (
  req: IncomingMessage,
  res: ServerResponse,
  next: (err?: unknown) => void,
) => void;
const logger = (format = Format.COMBINED): ExpressLogger => {
  const tokens: TokenIndexer = (morgan as unknown) as TokenIndexer;
  const getMessage = compile(format);

  return (req, res, next) => {
    const message = getMessage(tokens, req, res);
    next();

    let log = error;
    if (res.statusCode < 500) {
      log = warning;
    }

    if (res.statusCode < 400) {
      log = info;
    }

    if (message) {
      log(message);
    }
  };
};

export default logger;
