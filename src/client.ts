import {
  createLogger,
  transports,
  format,
  Logger,
  LeveledLogMethod,
} from 'winston';

const { Console } = transports;
const { combine, timestamp, json, prettyPrint } = format;

const isTestingEnvironment = process.env.ENVIRONMENT_FILE === 'testing';
const isDevelopmentEnvironment = process.env.ENVIRONMENT_FILE === 'development';

let ref: Logger;
const client = (): Logger => {
  if (!ref) {
    ref = createLogger({
      format: combine(
        timestamp(),
        isDevelopmentEnvironment
          ? prettyPrint({ depth: 10, colorize: true })
          : json({}),
      ),
      transports: [new Console({ silent: isTestingEnvironment })],
      exceptionHandlers: [new Console()],
      exitOnError: false,
    });
  }

  return ref;
};

export const info: LeveledLogMethod = (a: string | any, ...meta: unknown[]) =>
  client().info(a, ...meta);
export const warning: LeveledLogMethod = (
  a: string | any,
  ...meta: unknown[]
) => client().warn(a, ...meta);
export const error: LeveledLogMethod = (a: string | any, ...meta: unknown[]) =>
  client().error(a, ...meta);

export default client;
