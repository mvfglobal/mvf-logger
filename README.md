# MVF Logger

## Usage

Provides a consistent logging output for javascript applications.

### To install the package

Run `npm install @mvf/logger`

### How to use

Import log function of your choice and then use them to output a log message.

```ts
import { info, warning, error } from "@mvf/logger";

info("info message");
warning("info message");
error("info message");
```

The logger can also accept an object of extra data, which will be logged together with the message.

```ts
import { info } from "@mvf/logger";

info("info message", {
  clickId: "...",
  ...
});
```

### Exceptions

Logger is configured to catch any unhandled exceptions and output them to the console in a json format, consistent with the other log messages.

### Testing

Logger will be silent, it will not log anything, if `ENVIRONMENT_FILE` env variable is set to `testing`.

### Formatting

Logger will format output using `prettyPrint` formatter if `ENVIRONMENT_FILE` env variable is set to `development` in all other cases `json` formatter will be used to ensure consistent log output.

## Contributing

### Setup

- Run `make` to build the container
- Run `make shell` to enter the container
- Run `npm install` to install dependencies

Refer to package.json for commands

### After merging

After you have merged a PR to master, you need to rebuild and publish your changes.

1. Checkout master `git checkout master && git pull`
2. Use one of the following make publish commands to publish changes:
   - `make publish kind=patch` - Use this if your change is a bug fix and is backwards compatible.
   - `make publish kind=minor` - Use this if your change adds new functionality and is backwards compatible.
   - `make publish kind=major` - Use this if your change is not backwards compatible.
